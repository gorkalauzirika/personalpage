import http.server
import socketserver
import os
from mimetypes import types_map

class Handler(http.server.BaseHTTPRequestHandler):
    def do_GET(self):
        try:
            if self.path == "/":
                self.path = "index.html"
            elif self.path == "favico.ico":
                return
            
            if self.path[0] == "/":
                self.path = self.path[1:]
                
            fname,ext = os.path.splitext(self.path)
            if ext in (".html", ".css", ".jpg", ".png", ".svg"):
                with open(os.path.join(os.getcwd(),self.path),'rb') as f:
                    self.send_response(200)
                    self.send_header('Content-type', types_map[ext])
                    self.end_headers()
                    self.wfile.write(f.read())
                    return
        except IOError:
            self.send_error(404)

httpd = socketserver.TCPServer(("", 8000), Handler)

print("serving at port", 8000)
httpd.serve_forever()