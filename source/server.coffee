express = require 'express'
http = require 'http'
fs = require 'fs'
i18n = require "i18n"
app = do express
server = app.listen process.env.PORT || 8080

i18n.configure 
	locales: ['es','eu','en']
	cookie: 'locale'
	defaultLocale: 'es'
	directory: __dirname + '/locales'

app.configure ->
	app.set 'views',  __dirname + "/source/jade"
	app.set 'view engine', 'jade' 
	app.use "/css", express.static(__dirname + '/css') 
	app.use "/js", express.static(__dirname + '/js') 
	app.use "/images", express.static(__dirname + '/images') 
	app.use express.cookieParser() 
	app.use i18n.init 

app.get '/', (req, res) -> 
  res.render('index.jade') 

app.get '/:locale', (req, res) ->
  res.cookie 'locale', req.params.locale 
  res.redirect "/"
 
console.log 'Server started'  

setInterval ->
	options = 
		host: 'gorkalaucirica.herokuapp.com',
		port: 80,
		path: '/'
	
	http.get options, (res) ->
		console.log("Got response: " + res.statusCode)
	
, 20 * 60 * 1000	